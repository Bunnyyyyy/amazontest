import React from 'react';
import './App.css';
import Select from './Select';

class App extends React.Component {

  render() {
    const options = [
      { label: "Umang1", value: 1, image: 'https://cdn4.vectorstock.com/i/1000x1000/32/18/user-sign-icon-person-symbol-human-avatar-vector-12693218.jpg' },
      { label: "Umang2", value: 2, image: 'https://cdn4.vectorstock.com/i/1000x1000/32/18/user-sign-icon-person-symbol-human-avatar-vector-12693218.jpg' },
      { label: "Umang3", value: 3, image: 'https://cdn4.vectorstock.com/i/1000x1000/32/18/user-sign-icon-person-symbol-human-avatar-vector-12693218.jpg' },
      { label: "Umang4", value: 4, image: 'https://cdn4.vectorstock.com/i/1000x1000/32/18/user-sign-icon-person-symbol-human-avatar-vector-12693218.jpg' },
      { label: "Umang5", value: 5, image: 'https://cdn4.vectorstock.com/i/1000x1000/32/18/user-sign-icon-person-symbol-human-avatar-vector-12693218.jpg' }]
    return (
      <Select options={options} />
    );
  }
}

export default App;
