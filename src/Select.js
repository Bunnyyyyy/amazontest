import React from 'react';
import './App.css';

class Select extends React.Component {

    state = {}
    constructor(props) {
        super(props)
        this.state = {
            allUser: props.options,
            filteredUser: [],
            selectUser: [],
            highlighted: false
        };
    }
    componentDidMount() {
        document.addEventListener("keydown", (e) => this.KeyCheck(e));
    }
    KeyCheck(event) {
        const { highlighted, selectUser, allUser, inputValue } = this.state;
        var KeyID = event.keyCode;
        switch (KeyID) {
            case 8:
                if (selectUser.length && inputValue === "")
                    if (highlighted) {
                        var user = selectUser.pop();
                        allUser.push(user);
                        this.setState({
                            selectUser, allUser, highlighted: false
                        })
                    } else {
                        this.setState({
                            highlighted: true
                        })
                    }
                break;
            default:
                break;
        }
    }
    inputChange(e) {
        const { allUser } = this.state;
        var filteredUser = allUser;
        if (e.target.value) {
            filteredUser = allUser.filter(user =>
                user.label.toLowerCase().indexOf(e.target.value) > -1
            );
        } else {
            filteredUser = [];
        }

        this.setState({
            filteredUser,
            highlighted: false,
            [e.target.name]: e.target.value
        })
    }
    selectUser(newUser) {
        const { selectUser, allUser } = this.state;
        selectUser.push(newUser);
        allUser.forEach((user, index) => {
            if (user.value === newUser.value) {
                allUser.splice(index, 1)
            }
        })
        this.setState({
            selectUser,
            inputValue: '',
            allUser,
            filteredUser: []
        })
    }
    deleteUser(index) {
        const { selectUser, allUser } = this.state;
        allUser.push(selectUser[index]);
        selectUser.splice(index, 1);
        this.setState({
            selectUser, allUser, highlighted: false
        })
    }
    render() {
        const { inputValue, filteredUser, selectUser, highlighted } = this.state;
        return (
            <div className="App" >
                <div className="inputWrapperCss">
                    {selectUser.map((user, index) => (
                        <span className={`${highlighted && index === selectUser.length - 1 ? 'selectedUserBlockHighlited' : 'selectedUserBlock'}`}>
                            <img alt="" src={user.image} className="selectedUserImage" />
                            {user.label}
                            <span onClick={() => this.deleteUser(index)} className="closeIcon">X</span>
                        </span>
                    ))}
                    <input value={inputValue} name="inputValue" onChange={(e) => {
                        this.inputChange(e)
                    }} className="inputCss" />
                </div>
                <div >
                    {
                        filteredUser.map((user) => (
                            <div onClick={() => this.selectUser(user)} className="suggestionWrapper"><img alt="" src={user.image} className="imageCss" />{user.label}</div>
                        ))
                    }
                </div>
            </div >
        );
    }
}

export default Select;
